import 'package:bifa/models/Categorie.dart';
import 'package:bifa/models/Produit.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class ProduitController{

  String serverUrl = "https://www.app-bifa.com/api";
  int status ;
  String message ;
  List<Categorie> categories = List();
  List<Produit> produits = List();
  var token ;


  /**
   * recuperation des categorie du produit
   */
  Future<List> getProductCategorie() async{

    final prefs = await SharedPreferences.getInstance();
    final token = prefs.get('token');

    String myUrl = "$serverUrl/client/categorie";
    http.Response response = await http.get(myUrl,
        headers: {
          'Accept':'application/json',
          'Authorization' : 'Bearer $token'
        });
    var data =  json.decode(response.body);

    if (response.statusCode == 200) {
      categories = (data["data"]["categorie"] as List)
          .map((data) => new Categorie.fromJson(data))
          .toList();

      return categories;
    } else {
      throw Exception('Veuillez verifier votre connexion internet');
    }

  }


  /**
   * recuperation des produits par categorie
   */
  Future<List> getProduct(String categorieId) async{

    final prefs = await SharedPreferences.getInstance();
    final token = prefs.get('token');

    String myUrl = "$serverUrl/client/categorie/$categorieId/products";
    http.Response response = await http.get(myUrl,
        headers: {
          'Accept':'application/json',
          'Authorization' : 'Bearer $token'
        });
    var data =  json.decode(response.body);

    if (response.statusCode == 200) {
      produits = (data["data"]["produits"] as List)
          .map((data) => new Produit.fromJson(data))
          .toList();

      return produits;
    } else {
      throw Exception('Veuillez verifier votre connexion internet');
    }

  }


}


