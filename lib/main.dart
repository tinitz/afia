import 'package:bifa/views/invoices/list.dart';
import 'package:bifa/views/orders/checkout.dart';
import 'package:bifa/views/orders/shipping.dart';
import 'package:bifa/views/orders/versements.dart';
import 'package:bifa/views/products/list.dart';
import 'package:bifa/views/screens.dart';
import 'package:bifa/views/settings/changepassword.dart';
import 'package:bifa/views/settings/setting.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {


  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Bifa',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.red,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: 'Route',
      routes: {
        '/': (context) => LoginScreen(),
        'Home': (context) => HomePage(),
        'Route': (context) => RoutePage(),
        //'Panier': (context) => ShoppingCartPage(),
        'Commande': (context) => OrderPage(),
        'Livraison': (context) => ShippingPage(),
        'Versement': (context) => VersementPage(),
        'Product': (context) => ProductListPage(),
        'Parametre': (context) => SettingPage(),
        'Profil': (context) => ProfilPage(),
        'Facture': (context) => FacturePage(),
        'Notification': (context) => NotificationPage(),
        'ForgotPassword': (context) => ForgotPassword(),
        'ChangePassword': (context) => ChangePasswordPage(),
        'Checkout': (context) => CheckoutPage(),
      },
    );
  }
}


